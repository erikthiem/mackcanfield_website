require 'sinatra'
require 'csv'
require 'csv-mapper'
require 'sanitize'

include CsvMapper

TITLE = "Mack-Canfield Hall"

def generate_mack_frontdesk_items

	@mack_movies = Array.new
	@mack_cleaning = Array.new
	@mack_cooking = Array.new
	@mack_games = Array.new
	@mack_sports = Array.new
	@mack_misc_items = Array.new

	CSV.foreach("csv/frontdesk-mack.csv") do |row|
		movie_item, cleaning_item, cooking_item, games_item, sports_item, misc_item = row

		if !movie_item.nil?
			@mack_movies.push(movie_item)
		end

		if !cleaning_item.nil?
			@mack_cleaning.push(cleaning_item)
		end

		if !cooking_item.nil?
			@mack_cooking.push(cooking_item)
		end

		if !games_item.nil?
			@mack_games.push(games_item)
		end

		if !sports_item.nil?
			@mack_sports.push(sports_item)
		end

		if !misc_item.nil?
			@mack_misc_items.push(misc_item)
		end

	end

	# This eliminates the first item of the array which is the header and should not be displayed
	@mack_movies.shift
	@mack_cleaning.shift
	@mack_cooking.shift
	@mack_games.shift
	@mack_sports.shift
	@mack_misc_items.shift

end

def generate_canfield_frontdesk_items

	@canfield_movies = Array.new
	@canfield_cleaning = Array.new
	@canfield_cooking = Array.new
	@canfield_games = Array.new
	@canfield_sports = Array.new
	@canfield_misc_items = Array.new

	CSV.foreach("csv/frontdesk-canfield.csv") do |row|
		movie_item, cleaning_item, cooking_item, games_item, sports_item, misc_item = row

		if !movie_item.nil?
			@canfield_movies.push(movie_item)
		end

		if !cleaning_item.nil?
			@canfield_cleaning.push(cleaning_item)
		end

		if !cooking_item.nil?
			@canfield_cooking.push(cooking_item)
		end

		if !games_item.nil?
			@canfield_games.push(games_item)
		end

		if !sports_item.nil?
			@canfield_sports.push(sports_item)
		end

		if !misc_item.nil?
			@canfield_misc_items.push(misc_item)
		end

	end

	# This eliminates the first item of the array which is the header and should not be displayed
	@canfield_movies.shift
	@canfield_cleaning.shift
	@canfield_cooking.shift
	@canfield_games.shift
	@canfield_sports.shift
	@canfield_misc_items.shift

end

def generate_university_events

	events = import('csv/universityevents.csv') do

		start_at_row 1

		[title, date_time, description, link]

	end

	return events

end

def generate_hallcouncil_members

	people = import('csv/hallcouncilmembers.csv') do
		
		start_at_row 1

		[first_name, last_name, position, email, bio]
	
	end

	return people

end

get '/agent' do
	SUBTITLE = "Submit a Kill"
	erb :"agent/elimination"
end

get '/' do
	SUBTITLE = "Welcome to the 2013-2014 School Year"
	erb :home
end

get '/mackcanfieldevents' do
	SUBTITLE = 'Mack-Canfield Events'
	erb :mackcanfieldevents
end

get '/universityevents' do
	@university_events = generate_university_events
	SUBTITLE = 'University Events'
	erb :universityevents
end

get '/pictures' do
	SUBTITLE = 'Pictures'
	erb :pictures
end

get '/mackcanfieldhallcouncil' do
	@hallcouncil_members = generate_hallcouncil_members
	SUBTITLE = 'Mack-Canfield Hall Council'
	erb :mackcanfieldhallcouncil
end

get '/staff' do
	SUBTITLE = 'Staff'
	erb :staff
end

get '/frontdesk' do

	generate_mack_frontdesk_items
	generate_canfield_frontdesk_items

	SUBTITLE = 'Front Desk'
	erb :frontdesk
end

get '/contact' do
	SUBTITLE = 'Contact'
	erb :contact
end

get '/apply' do
	SUBTITLE = 'Application for Hall Council'
	erb :hallcounciltoolate
end

get '/idea' do
	SUBTITLE = 'Suggestions for Cool Events'
	erb :suggestions
end

=begin
post '/apply' do
	require 'pony'

	body = "<b>Name:</b> " + Sanitize.clean(params[:name]) + "<br><br><b>Email:</b> " + Sanitize.clean(params[:email]) + 
		"<br><br><b>Major(s) / Minor(s)</b>: " + Sanitize.clean(params[:major_minor]) + "<br><br><b>Year:</b>" + Sanitize.clean(params[:year]) +
		"<br><hr><br><b>Question: Have you ever served on a student organization in the past? Tell us about it.</b><br>" +
		"<br>Answer: " + Sanitize.clean(params[:question1]) + 
		"<br><hr><br><b>Question: Why are you a good hall council candidate?</b><br>" +
		"<br>Answer: " + Sanitize.clean(params[:question2]) +
		"<br><hr><br><br><b>Question: Give an example of a time when you were part of a successful team.</b><br>" +
		"<br>Answer: " + Sanitize.clean(params[:question3]) +
		"<br><hr><br><br><b>Question: What are you looking to gain from this experience?</b><br>" +
		"<br>Answer: " + Sanitize.clean(params[:question4]) +
		"<br><hr><br><br><b>Question: Give a suggestion for a fun Mack-Canfield event.</b><br>" +
		"<br>Answer: " + Sanitize.clean(params[:question5])
	
	Pony.mail(
		:from => 'applicant@mackcanfield.com',
		:to	=> 'erikthiem@gmail.com',
		:subject => 'Hall Council Application',
		:headers => { 'Content-Type' => 'text/html' },
		:body => body,
		:port => '587',
		:via => :smtp,
		:via_options => {
			:address	=> 'smtp.sendgrid.net',
			:port			=> '587',
			:enable_starttls_auto => true,
			:user_name => ENV['SENDGRID_USERNAME'],
			:password	=> ENV['SENDGRID_PASSWORD'],
			:authentication => :plain,
			:domain => ENV['SENDGRID_DOMAIN']
		})

	erb :apply_message

end
=end

post '/suggestions' do
	require 'pony'

	body = "<b>Name:</b> " + Sanitize.clean(params[:name]) + "<br><br><b>Email:</b> " + Sanitize.clean(params[:email]) +
		"<br><br><b>Event suggestion:</b><br>" + Sanitize.clean(params[:suggestion])
	
	Pony.mail(
		:from => 'suggestor@mackcanfield.com',
		:to	=> 'erikthiem@gmail.com',
		:subject => 'Event Suggestion',
		:headers => { 'Content-Type' => 'text/html' },
		:body => body,
		:port => '587',
		:via => :smtp,
		:via_options => {
			:address	=> 'smtp.sendgrid.net',
			:port			=> '587',
			:enable_starttls_auto => true,
			:user_name => ENV['SENDGRID_USERNAME'],
			:password	=> ENV['SENDGRID_PASSWORD'],
			:authentication => :plain,
			:domain => ENV['SENDGRID_DOMAIN']
		})

	erb :suggestion_confirmation

end


