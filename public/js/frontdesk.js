$(document).ready(function() {

	$(".frontdesk_category").click(function(event) {

		event.preventDefault();

		$(this).next().slideToggle(1000);

		$('html, body').animate({

			scrollTop: $(this).offset().top
			
		}, 1000);

	});

});
